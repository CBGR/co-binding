# This script will check for the closest genomic distance between two transcription
# factor (TF) binding sites (TFBSs) from two distinct TFs. It assumes that in the
# input folder each file corresponds to the **unique** TFBSs of a TF and that the 
# extension of the file is .bed. The input files should have at least the first 
# three columns of a BED file, namely chromosome, start and end. It will run through
# all the files and retrieve pairwise the closest genomic distance between each two
# TFBSs in the input file, and write the output to a file who's name will be explicit
# for the pair of TFs tested.
#
# Important: in this analysis we have used the center of the TFBS only, not the entire
# genomic region covered by the TFBS
#
# Dependencies:
#-------------
#
# bedtools
#
# NOTE: (again) the TFBSs in the input files should be unique
#
#parse the arguments
in_dir=
if [ -z "$1" ]
    then
        echo "No input folder specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi

out_dir=
if [ -z "$2" ]
    then
        echo "No output folder specified. Exiting...";
        exit 1
    else
        out_dir=$2
fi

mkdir -p $out_dir"/closest_distance"
cd $in_dir   

filesA=(`find $d -name "*.bed"`)
# per TFBS file
for fileA in "${filesA[@]}"
    do
       echo "$(date)    Processing file: $(basename $fileA)"
       # and all TFBS files in that folder
       filesB=(`find $folder -name "*.bed"`)
       for fileB in "${filesB[@]}"
           do
              # avoid computing the relative distance on itself
              if [ "$(basename $fileA)" != "$(basename $fileB)" ]
                 then
                    #and calculate closest
                    bedtools closest -d -a $fileA -b $fileB | \
                    awk -v OFS='\t' '{print $7}' > $out_dir"/closest_distance/"$(basename $fileA)"_vs_"$(basename $fileB)
              fi
       done
done           
