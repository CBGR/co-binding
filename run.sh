#!/bin/bash
#This script will launch the co-binding analysis. It assumes you have all the needed files (null distributions and a folder withe unique transcription factor binding sites per TF) ready. Refer to individual scripts in utils or src for a more detailed description.
#
#Dependencies:
#------------
#
# bedtools
# R statistical environment
#
# ./src/get_closest_pairwise.sh
# ./get_pvalue_pairwise.R
# ./process_pvalues.R
#
#
# usage: bash run.sh /path/to/my/unique/TFBS/files /path/to/my/output/folder

#modify accordingly if you change the location/name of the files
REL_PATH="./src/"

call_pvalue_calc_script=$REL_PATH"call_pvalue_calculation.R"
pvalue_calc_script=$REL_PATH"get_pvalue_pairwise.R"

call_pvalue_proc_script=$REL_PATH"call_pvalue_processing.R"
pvalue_proc_script=$REL_PATH"process_pvalues.R"

null_distributions="./utils/null_distributions_uniform_intervals.tsv"

# parse arguments
in_dir=
if [ -z "$1" ]
    then
        echo "No input folder specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi

out_dir=
if [ -z "$2" ]
    then
        echo "No output folder specified. Exiting...";
        exit 1
    else
        out_dir=$2
fi

# Let's do this!!
source $REL_PATH/get_closest_pairwise.sh

# Step 1
echo "Getting the closest distance for all pairs of TFs in the input folder..."
bash $REL_PATH/get_closest_pairwise.sh $in_dir $out_dir

# Step 2
echo "Computing p-values for every pair of TFs..."
$(Rscript $call_pvalue_calc_script $pvalue_calc_script $in_dir $out_dir"/closest_distance" $null_distributions $out_dir"/pairwise_pvalues.tsv")

# Step 3
echo "Processing p-values..."
$(Rscript $call_pvalue_proc_script $pvalue_proc_script $out_dir"/pairwise_pvalues.tsv" $out_dir)

echo "Done."
